import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const imageElement = createFighterImage(fighter);
  const nameElement = createFighterName(fighter);
  const healthElement = createFighterHealth(fighter);
  const attackElement = createFighterAttack(fighter);
  const defenseElement = createFighterDefense(fighter);

  fighterElement.append(imageElement, nameElement, healthElement, attackElement, defenseElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterName(fighter) {
  const { name } = fighter;
  const nameElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
  });

  nameElement.innerText = `Name: ${name}`;

  return nameElement;
}

export function createFighterHealth(fighter) {
  const { health } = fighter;

  const healthElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
  });

  healthElement.innerText = `Health: ${health}`;

  return healthElement;
}

export function createFighterAttack(fighter) {
  const { attack } = fighter;

  const attackElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
  });

  attackElement.innerText = `Attack: ${attack}`;

  return attackElement;
}

export function createFighterDefense(fighter) {
  const { defense } = fighter;

  const defenseElement = createElement({
    tagName: 'span',
    className: 'arena___fighter-name',
  });

  defenseElement.innerText = `Defense: ${defense}`;

  return defenseElement;
}
