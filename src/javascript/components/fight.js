import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    document.addEventListener('keydown', logKey);

    function logKey(e) {
      const key = e.code;

      if (key === controls.PlayerOneAttack) {
        document.addEventListener('keydown', secondLogKey);

        function secondLogKey(e) {
          const key = e.code;

          if (key === controls.PlayerTwoBlock) { 

            const damage = getDamage(firstFighter, secondFighter);
            const fighterHealth = secondFighter.health > damage? secondFighter.health - damage : 0;

            secondFighter.health = fighterHealth;

            const healthBar = document.getElementById('right-fighter-indicator');
            healthBar.style.width = `${secondFighter.health}%`;

            if ( secondFighter.health === 0) {
              resolve({...firstFighter}) ;
            } 

          document.removeEventListener('keydown', secondLogKey);

          } else if (key === controls.PlayerTwoAttack) {
            const secondFighterDamage = getHitPower(firstFighter);
            const firstFighterDamage = getHitPower(secondFighter);

            secondFighter.health = secondFighter.health > secondFighterDamage? secondFighter.health - secondFighterDamage : 0;
            firstFighter.health = firstFighter.health > firstFighterDamage? firstFighter.health - firstFighterDamage : 0;

            const rightHealthBar = document.getElementById('right-fighter-indicator');
            rightHealthBar.style.width = `${secondFighter.health}%`;

            const leftHealthBar = document.getElementById('left-fighter-indicator');
            leftHealthBar.style.width = `${firstFighter.health}%`;

            if ( secondFighter.health === 0) {
              resolve({...firstFighter}) ;
            } else if (firstFighter.health === 0) {
              resolve({...secondFighter}) ;
            }

          }

        }

      }

      if (key === controls.PlayerTwoAttack) {
        document.addEventListener('keydown', secondLogKey);

        function secondLogKey(e) {
          const key = e.code;

          if (key === controls.PlayerOneBlock) { 
            const damage = getDamage(secondFighter, firstFighter);
            const fighterHealth = firstFighter.health > damage? firstFighter.health - damage : 0;

            firstFighter.health = fighterHealth;

            const healthBar = document.getElementById('left-fighter-indicator');
            healthBar.style.width = `${firstFighter.health}%`;

            if ( firstFighter.health === 0) {
              resolve({...secondFighter}) ;
            }

            document.removeEventListener('keydown', secondLogKey);
          } else if (key === controls.PlayerOneAttack) {
              const secondFighterDamage = getHitPower(firstFighter);
              const firstFighterDamage = getHitPower(secondFighter);

              secondFighter.health = secondFighter.health > secondFighterDamage? secondFighter.health - secondFighterDamage : 0;
              firstFighter.health = firstFighter.health > firstFighterDamage? firstFighter.health - firstFighterDamage : 0;

              const rightHealthBar = document.getElementById('right-fighter-indicator');
              rightHealthBar.style.width = `${secondFighter.health}%`;

              const leftHealthBar = document.getElementById('left-fighter-indicator');
              leftHealthBar.style.width = `${firstFighter.health}%`;

              if ( secondFighter.health === 0) {
                resolve({...firstFighter}) ;
              } else if (firstFighter.health === 0) {
                resolve({...secondFighter}) ;
              }

          }
        }

      }

      if (key === controls.PlayerOneBlock || key === controls.PlayerTwoBlock) {
        return;
      }

    }

    let map = {};

    onkeydown = onkeyup = function(e){

      map[e.code] = e.type == 'keydown';

      let pressedButtons = [...Object.keys(map)];

      const filteredButtons = pressedButtons.filter(button => button ==='KeyQ' || button ==='KeyW' ||
          button ==='KeyE' || button ==='KeyU' || button ==='KeyI' || button ==='KeyO'
      );


      const firstObjectAllFalse = !map.KeyQ && !map.KeyW && !map.KeyE;
      const secondObjectAllFalse = !map.KeyU && !map.KeyI && !map.KeyO;

      const firstFighterIsTrue = arraysEqual(filteredButtons, controls.PlayerOneCriticalHitCombination);
      const secondFighterIsTrue = arraysEqual(filteredButtons, controls.PlayerTwoCriticalHitCombination);

      if (firstFighterIsTrue && firstObjectAllFalse) {

        const damage = 2*getHitPower(firstFighter);
        const fighterHealth = secondFighter.health > damage? secondFighter.health - damage : 0;

        secondFighter.health = fighterHealth;

        const healthBar = document.getElementById('right-fighter-indicator');
        healthBar.style.width = `${secondFighter.health}%`;

        if ( secondFighter.health === 0) {
          resolve({...firstFighter}) ;

        }

        pressedButtons = [];
        for (const prop of Object.getOwnPropertyNames(map)) {
          delete map[prop];
        }

        return;

      }

      if (secondFighterIsTrue && secondObjectAllFalse) {

        const damage = 2*getHitPower(secondFighter);
        const fighterHealth = firstFighter.health > damage? firstFighter.health - damage : 0;

        firstFighter.health = fighterHealth;

        const healthBar = document.getElementById('left-fighter-indicator');
        healthBar.style.width = `${firstFighter.health}%`;

        if ( firstFighter.health === 0) {
          resolve({...secondFighter}) ;

        }

        pressedButtons = [];
        for (const prop of Object.getOwnPropertyNames(map)) {
          delete map[prop];
        }

        return;

      }
    }

  });
}

export function getDamage(attacker, defender) {
  // return damage

  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = (hitPower - blockPower) > 0? hitPower - blockPower : 0;

  return damage;
}

export function getHitPower(fighter) {
  const {attack} = fighter
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;

  console.log(criticalHitChance);

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const {defense} = fighter
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;

  console.log(dodgeChance);

  return power;
}

function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  a.sort();
  b.sort();

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}