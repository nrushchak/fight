import {showModal} from './modal';
import {createFighterName} from '../../components/fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
 
  const fighterName = createFighterName(fighter);

  fighterName.style.webkitTextFillColor = 'green';
  fighterName.style.display = 'flex';
  fighterName.style.justifyContent = 'center';
  fighterName.style.alignItems = 'center';
  fighterName.style.padding = '50px';

  showModal({title: 'winner is:', bodyElement: fighterName });
}
